# Resiliency

## 1. Inter-Service communication errors

**Service Fails to communicate to Database (Firestore/Postgres)** - in runtime, when processing requests application will fail to query the database. 
_Desired behaviour_: if application cannot establish a connection to the database at start up, it should log an error and exit. This way, possible misconfiguration can be noticed early on and rollout is prevented by Kubernetes.\
If connection is lost during runtime, then healthcheck should fail and Kubernetes will restart containers. Clients should follow internal server error flow.

**Auth service fails to save newly created user to Firestore** - we should retry writing the document 5 times with a delay of 1 second. Each attempt should be logged and written to metrics. If all attempts fail, we should send internal server error back to the client.

**Upload service fails to write uploaded file to Google Cloud Storage** - we should retry writing the file 5 times with a delay of 1 second. Each attempt should be logged and written to metrics. If all attempts fail, we should send internal server error back to the client. There was an option to save this file to local storage and upload it later, but we decided not to do so as it makes design more complex and handling this on client side seems to be more correct.

**Upload service fails to communicate to songs service** - we should retry request up to 5 times with an exponentially increasing delay (2 - 30 seconds) + jitter. If song service produces a lot of errors in general, we should block percentage of new requests to upload service with "Service Unavailable" response.

**Playback service fails to communicate to Redis cache** - after 3 attempts with 1 second delay we should close incoming connection. As there are more errors communicating to Redis cache, we should reduce the number of connections we accept in the first place with "Service Unavailable" response.

**Playback service fails to communicate to recomendation engine service** - after a single retry we should fallback to showing user recent history instead of notifications. For that, we will perform request to the history api.

**Upload service fails to publish message to Pub/Sub** - we should retry once again immediately and if we fail again, we can ignore the failure as it is not critical for the system and can be triggered later by user (such as lyrics fetching).

**Monitoring service fails to communicate to one or multiple targets** for more than 1 hour - write to log and alert.

## 2. Service misconfiguration

**Token Sign Secret is not set** - in runtime, when starting session application cannot sign a jwt token and
any request cannot be satisfied.\
_Desired behaviour_: the secret is checked at application start. If the secret is not defined, application should log an error and terminate the execution. This way, misconfiguration can be noticed early on. Containers failing to start will prevent Kubernetes from rolling out a new revision.

**Upload service was not set with a GCS bucket to upload to** - same as above, exit on container start with proper messages in the log.

## 3. Client-Server communication

**Request to auth fails due to internet connectivity issues** - we should retry one time after 2 seconds and if the issue persists, we should notify a user with an error message.

**Request to song api fails due to internet connectivity issues** - we should retry requests indefinitely every 30 seconds until connection is restored.

**User upload fails (due to internet connectivity issues)** - we should place file back into upload queue and retry immediately. If we fail 5 times in a row, we should stop attempts and notify user that uppload for this particular file has failed.

**Client fails to open websocket to playback service due to internet connectivity issues or service outage** - client should fallback to managing playback and queue locally, while performing connection attempts in the background. Exponential backoff should be used (2 - 30 seconds) and jitter.

## 4. Frontend

**User accesses frontend using unsupported browser or has javascript disabled** - unsupported browsers can be detected on server side using `User Agent` header. If browser is not supported anymore, NGINX should serve a special page with upgrade instructions. If user has javascript enabled, we should show notice that js is required using `noscript` html tag.

**User accesses page which does not exist** - we show 404 page and log and meter this access attempt.

**Song download fails (due to connectivity or internal server error)** - we should retry one time and if we fail again, we should show a notification to the user and skip to the next song. 

**Any request fails due to internal server error** - we should retry once immediately, and if it fails, we should show notification to the user and cancel the action. However, if action is syncable (like adding song to the playlist), we should keep trying in background every 10 minutes for an hour. If we do not succeed, this action should be reverted.

## 5. Communication to external services

**Lyrics service fails to provide lyrics to the song (service error/network issues)** - we should put this song to retry queue on Pub/Sub and try again later after 1 minute, and 6 hours (4 times). If we fail after 5 attempts, we should save this song to the table of failed fetches for further investigation.

**Lyrics service fails to provide lyrics to the song (not found)** - we should set the `lyrics` field in the database as `unknown`. And, possibly, send a notification to the user (this will be determined later based on how of this will be the case).

**Telegram Bot fails to communicate to telegram api** - in such case we should put a message to retry queue on Pub/Sub and try again later. Each attempt should be logged and metered. Give up after 2 attempts with 10 minutes in between and 3 attempts with 6 hours in between.

**Telegram fails to call bot webook** - retries are managed by telegram. If issue was on our side, we can force retries by resetting the hook.

## 6. Flows

![upload flow](resources/upload-flow.png)

![playback flow](resources/playback-flow.png)

![auth flow](resources/auth-flow.png)
