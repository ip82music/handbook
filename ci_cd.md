# CI/CD Tools

## Gitlab CI
We use [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) because:
- We already use Gitlab to host our repos.
- Graph-based approach to pipelines.
- Configurable and flexible, stages run in docker containers.
- We can host our own runners to reduce the cost and to improve the performance.
- Other solutions are more difficult to setup/integrate, while brining less features.

We host our **own runners** outside of GCP to separate CI environment from production. It is deployed on Kuberenetes and runs workloads as pods. [minio](https://min.io/) is used for caching (having cache close to actual runners makes it really fast to download/upload cache, while keeping it distributed).

## We ship Docker images
All our artifacts are docker images. Docker images are pushed/pulled to/from [Gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/). We do not use GCR.

We use Kubernetes on GKE to run backend services, so docker image is the only artifact we ship. 

For frontend deployments, we used to just upload all the files to GCS bucket. Later we switched to building NGINX image and deploying it alongside backend services on GKE. This simplifies handling routes and errors, makes rollbacks a bit easier, simplifies deploys and improves the performance a bit.

Telegram bot is just another backend service, so it is shipped as docker image as well.

We also have some pre-built images stored in `infrastructure` repo registry.

Our infrastructure is defined as `yaml` files which are applied with kubectl. We use Helm only for couple of external components, which were deployed manually (such as gitlab runner).

Initially we used [fluxcd](https://fluxcd.io) to automatically deploy latest images. But it turned out that it actually makes deploy more complex. There are periods of time when actual infrastructure does not match the one defined in `yaml` due to lag/errors during apply. The later is a huge problem, because errors are only visible in fluxcd logs.

That's why we are transitioning to `yaml` files which are applied from the ci with `envsubst` to template version/environment. At the same time our use case is too simple to require Kustomize/Helm for deploys.

# Branching strategy

All our repos have master branch and feature branches. We build a web application, so there is no need for us to support old releases and there is no point in having realease branches.

We deploy new versions right after a new commit is pushed to the `master` branch. We don't think having `dev` branch and being careful about releases is worth it on this project. With CI/CD tools perfected we prefer to iterate fast and it is okay for us to occassionally break things. When things go terribly wrong, we revert to a previous version just by running previous pipeline on gitlab (also, there is an environment feature that helps).

Hotfix? Just push it to the `master`.

There is a feature we would not like to be immediately deployed? We place it behind a feature flag, which uses environment variable value. This feature gets deployed immediately, but we enable it when needed by updating `yaml` file. It can also be enabled for test users by checking `features` field in jwt token.

In other words, at any moment of time, `master` branch is considered deployable.

This applies to backend, frontend and telegram bot. Approach is universal here.

# Environments

Backend, frontend and tgbot have **production** environment, which contains the latest build from `master`. 

For each feature there is an option to deploy to a `feature` environment. This environment is to test a feature before merging to `master`. It is used for QA purposes as well, and given we deploy right after the merge, we do not need QA environment. Feature environment is really close to actual production environment. The only difference is the namespace it is deployed into. We use [kube janitor](https://codeberg.org/hjacobs/kube-janitor) to delete old depoyments after some time.

**We do have a canary environment.** Canary environment is a single environment shared between feature branches, unlike feature environments. Build can be manually deployed there and tested on a percentage of live production traffic. Canary environment is overwritten when deploying to prod (to keep canary up to date).

**We do not have preprod environment.** Remember that every new feature is instantly deployed to prod and disabled by feature flag if needed? To test this new feature from frontend side or other service side we can just issue a jwt token with this feature enabled.

**We do not have stage environment**, because (again) we deploy from master after each merge. We do not want to be that careful about releases. Our CI tooling should prevent shipping broken code. Rolling updates help in a certain way too. Reverting to the previous release (any previous release back in time) is a simple last resort solution.

**We do not have a CI environment**. While under test, each service uses production instances of other services to communicate. Getting all the system up on a CI runner is not an easy task, so we don't. Also, we have test data in our prod database (see next section to understand why we do).

All our environments are separated using Kubernetes namespaces. One could say that having separate Kubernetes clusters is a better solution, but in fact it introduces extra maintainence and infrastructure costs which are not worth it. You know, one big cluster is much more fun than a few smaller ones.

# Quality Gates

We rely on high (95%+ of all use cases) coverage with black box integration tests to check that a backend service has an expected behaviour.

For frotend we use e2e tests, which we run against `feature` and `prod` deployments. It is an equivalent to black box integration tests and we also target 95%+ use case coverage here. 

We do not aim for high unit test coverage and only use it for parts with complex logic.

We use `eslint` to check that our code is clean and matches Google JavaScript Styleguide.

We use `stryker` to check test quality. But it is run manually and has no effect on CI pipeline.

We use `lighthouse` to measure frontend quality. It can be run against `prod` and `feature` environments with diffs available. It has no effect on CI pipeline, run manually and is used as reference only.

Finally, we use `k6` for perfromance testing backend services. It also runs against `prod` and `feature` environments and just like `lighthouse` it is a CI manual step and has no effect on CI pipeline.

# Documentation

You are actually reading it right now. We call it a handbook repo and it is a shared knowledge base. We use markdown for text docs and swagger files for api specs.

We have documentation in git repo to review it just like we review the code and to have history easily available.

We don't keep any documentation in the code or anywhere close to it. Simple things should be self-documenting, while complex should be described in this repo.

The point in sharing docs is to make them accessible and editable not only be developers, but also by clients. Consider a frontend developer who finds some section of backend api documentation unclear. There is no need to ask backend devs to update documentation. Instead, they can submit MR to this repo without having to download the repo with the source code. Documentation quality increases when consumers are involved into writing it.

# Infrastructure

We have terraform files in our `infrastructure` repo.

We run `terraform plan` and `terraform apply` on CI. There are two reasons we don't run terraform locally:
1. To prevent changes to state from not beeing committed. Repo can't be out of sync with actual state if all changes are made on CI.
2. To review all changes to infrastructure the same way we review code. Changes are applied only after merge request is approved and merged into `master`.

# Stateful services (postgres)

Stateless services are easy to deploy and to revert. But when it comes to the state in postgres database things start to get tricky.

We should keep number of backwards-incompatible changes to database schema as low as possible. Every time we do such a change (i.e. delete existing column or rename it), we make it harder to rollback.

We also share a single database between all environments. That is dirty, but easier to maintain.

We really need to think about database migrations on CI and having a separate database for each environment (it is okay to have them all on single Cloud SQL instance though).

# Workflow from the developer standpoint

It does not matter if developer is working on backend or frontend, the workflow is basically the same:

1. New branch is created for new feature (from `master`).
2. New code and tests are committed to that branch.
3. Branch is pushed to Gitlab and Merge Request (MR) is created.
4. CI for branch is automatically started. It runs linter, unit tests, integration tests, e2e tests. All those run in parallel. At the same time, in a separate step we check that code actually builds.
5. If code builds, new docker image is created and pushed to registry.
6. At this point, developer can manually deploy to `feature` or `canary` environments (like to `feature` environment is available in build logs).
7. Developer can run lighthouse or performance tests against `feature` environment and compare it to prod results. 
8. After merge request is approved, it is merged into `master`.
9. On master, deploy to `prod` and `canary` is started right after all tests pass and docker image is built.
10. Optionally, after deploy is finished, additional step can clear caches (Cloudflare for example).
11. As a manual step, `lighthouse` or performance tests can be run against prod environment.

# Workflow for hotfix

Here is what you do when things are messed up:

First, use gitlab pipeline or environment interface to deploy a previous release, this should help while you are working on a fix. If it is a new feature that broke everything, you can try disabling it with a feature flag (set environment variables in `yaml` in service or  infrastructure repo).

Then, evaluate how much time and testing you need for a fix. If it is really a hot fix, then follow the default workflow for new features and fixes.

If you need more time or find yourself blocking other developers, perform `git revert` and submit a reverting change MR. After that follow the default workflow for a proper fix.

In case of emergency, it is okay to push to master (but no history rewriting!) - this reduces time to deploy a fix.