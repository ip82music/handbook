# Storage

## General Info

Uploaded files are stored in `ip82music_music_files` [bucket](https://console.cloud.google.com/storage/browser/ip82music_music_files;tab=objects?forceOnBucketsSortingFiltering=false&project=ip82music&prefix=&forceOnObjectsSortingFiltering=false).

Max uploaded file size: 100MB.

Each uploaded file is named by a generated UUID.

## Upload File Flow
Request:
```
POST https://api.musicstream.app/v1/files

Headers: x-access-token containing jwt token
Body is the file being uploaded.
```

Response:

If everything is OK:
```
Status code: 200

Body:
{
    "id": "uuid"
}
```

If file is too large:
```
Status code: 400

Body:
{
    "error": "FILE_TOO_LARGE",
    "message": "File size is larger than a limit for this upload: 100MB"
}
```

Possible errors are: `FILE_TOO_LARGE`, `NO_BODY`.

If user is not authorized (no `x-access-token` header set):
```
Status code: 403

Body:
{
    "error": "UNAUTHORIZED",
    "message": "Access token is not set"
}
```

If internal server error than same as above, but with status code 500 and error 'INTERNAL_SERVER_ERROR'.    