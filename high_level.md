# System Overview

![High Level Diagram](./resources/2.png)

To run our services we use Kubernetes deployed on GKE. 
The reasoning behind Kubernetes is that even for small projects it is more convenient to use than just running docker containers on a single server. The second reason is that we want to get some hands-on experience with Kubernetes. We chose GKE as it is the best managed Kubernetes offering.

There are four main services processing user requests: Authorization, Playback, Songs & Playlists, Storage.
Three is also Cloud Storage Proxy service which does not have any domain logic, but just proxies requests to frontend cloud storage buckets.

We use HTTP for communication between clients and backend. Connection is proxied by Cloudflare, which adds SSL encryption. We use L4 pass-through load balancer, so http connection is terminated at the target machines.

We keep NGINX ingress controller in front of all services to route traffic based on request host and to add headers.

Auth service is responsible for user registration and login. It issues JWT tokens. User data is stored in Firestore.

Playback service contains information about the songs beeing played in current session, song history and next songs to play. It uses Redis for sync and shared postgres database to access information about songs and playlist and to record stats.

Songs & Playlists service provides an API for client to access and edit songs and playlist entries. This includes changing song name, author, album, creating playlist, arranging songs into it. This service stores its infromation in postgres database.

Storage service manages music files uploads. Uploaded files are saved to Google Cloud storage bucket. To store information about uploads, postgres database is used.

Cloud Storage Proxy proxies requests to Google Cloud Storage bucket, based on proxy configuration.

Services talk to Google Cloud IAM to get access tokens for API calls to cloud services.

We also use Prometheus as monitoring service. It accesses all the services to collect metrics from them.