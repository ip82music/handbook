# Playlists

## General Info

Playlists are stored in postgres `playlists` table.

To store which playlist contains what we have `playlist_songs` table.

All playlist-related queries are handled by [API service](https://gitlab.com/ip82music/api).

## Playlist properties

Each Playlist has the following properties:
- `id: uuid` - we do not numeric autoincrement ids to allow us to make client apps work offline if we will want to.
- `name: string`
- `owner: uuid` - id of the user who created this MR
- `public: bool` - is this playlist public or private to this user

Each `PlaylistEntry` contains the following properties (we store it in `playlist_songs`):
- `playlist_id: uuid`
- `song_id: uuid`
- `position: number` - position of the song in the playlist

## Create Playlist Flow
Access tokens are set in `x-access-token` headers.

Request:
```
POST https://api.musicstream.app/v1/playlists

{
    "name": "some playlist name",
    "public": false,
    "songs": [
        "first-song-uuid",
        "second-song-uuid"
    ]
}
```
(`songs` and `public` fields are optional)

Response if everything is OK:
```
Status code: 200

Body:
{
    "id": "playlist-uuid"
}
```

If user is not authorized (no `x-access-token` header set):
```
Status code: 403

Body:
{
    "error": "UNAUTHORIZED",
    "message": "Access token is not set"
}
```

If internal server error than same as above, but with status code 500 and error 'INTERNAL_SERVER_ERROR'.

## Get all user playlists
Request:
```
GET https://api.musicstream.app/v1/playlists
```

Response if everything is OK:
```
Body:
{
    "playlists": [{
        "id": "playlist-uuid"
        "name": "first playlist",
        "public": false,
        "songs": [
            "first-song-uuid",
            "second-song-uuid"
        ]
    }]
}
```

## Get public playlists
Request:
```
GET https://api.musicstream.app/v1/playlists/public?limit=500&offset=1000
```

Max limit value is `500`.

Response if everything is OK is the same as for user playlists.

## Add song to playlist flow
```
PUT https://api.musicstream.app/v1/playlists/playlistID/songs

{
    "songs": [
        "first-song-uuid"
    ]
}
```

## Remove song from playlist flow
```
DELETE https://api.musicstream.app/v1/playlists/playlistID/songs/songID
```

## Delete playlist flow
```
DELETE https://api.musicstream.app/v1/playlists/playlistID
```
