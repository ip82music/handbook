# Auth

## General Info

User data is stored in Firestore ([link to GCP console](https://console.cloud.google.com/firestore/data/users?project=ip82music)).

Sign Up, Login and all user profile-releated queries are handled by [Auth service](https://gitlab.com/ip82music/auth). Auth service issues JWT tokens which are then used by other services to verify user identity.

## User properties

Each User has the following fields:
- `id: uuid` - We do not use numeric autoincrement ids because those are not something that you would use with database like Firestore.
- `email: string` - we don't use usernames. We also don't use `email` as primary identifier to make it possible to be changed by user. The requirement to email is to contain alphanumeric characters, '@' and a domain name afterwards. Max length: 250 characters.
- `password: string` - hashed (on server side). Password should be at least 6 characters with no upper limit.
- `telegram_id: number?` (*optional*) - id of telegram account associated with this user. Set by frontend, used by telegram bot. No validation required.
- `sessions: Session[]` - all currently active sessions. Should not contain more than 5 entries (old entries should be removed).

`Session` type:
- `id: uuid` - used as refresh token.
- `name: string` - session name. Contains browser/mobile device name.
- `valid_until: number` - timestamp util which this session is valid.

## Tokens

We use JWT tokens. There are two tokens: access token and refresh token ([read more here](https://jwt.io/introduction/) and [here](https://gist.github.com/zmts/802dc9c3510d79fd40f9dc38a12bccfc)).

`refreshToken` is a uuid of session valid for 30 days. For browser frontend it is persisted to http only cookie. For mobile it is should be saved to some kind of protected persistent storage.

`accessToken` is a JWT token valid for 1 hour. It should not be persisted to any kind of storage, but always kept in memory.
Auth service should sign this token with a secret available in `AUTH_SECRET` environment variable. It will be provided to a container by Kubernetes.

`accessToken` contents:
```
{
    "sub": "user_uuid",
    "name": "user email",
    "valid_until": timestamp
}
```

## Hashing Passwords

```
password_hash = argon2(sha512(password))
```

SHA512 protects from DOS attack when user sends us a really long password. Argon2 provides security.

## Sign Up Flow
Request:
```
POST https://auth.musicstream.app/v1/signup

{
    "email": "user@example.com",
    "password": "qwerty"
}
```

Response:

If everything is OK:
```
Status code: 200
Set-cookie: refreshToken=..., httpOnly; expirationTime=...

Body:
{
    "accessToken": "...",
    "refreshToken": "..."
}
```
Refresh token should be saved to session list. If there are too many sessions already, then oldest one should be removed.

If validation failed:
```
Status code: 400

Body:
{
    "error": "PASSWORD_TOO_SHORT",
    "message": "Password is too short: should be at least 8 characters long"
}
```

Possible errors are: `PASSWORD_TOO_SHORT`, `EMAIL_INVALID`.

If internal server error then same as above, but with status code 500 and error `INTERNAL_SERVER_ERROR`.

## Login Flow
Request:
```
POST https://auth.musicstream.app/v1/login

{
    "email": "user@example.com",
    "password": "qwerty"
}
```

Response:

If everything is OK:
```
Status code: 200
Set-cookie: refreshToken=..., httpOnly; expirationTime=...

Body:
{
    "accessToken": "...",
    "refreshToken": "..."
}
```
Refresh token should be saved to session list. If there are too many sessions already, then oldest one should be removed.

If auth failed:
```
Status code: 400

Body:
{
    "error": "USER_NOT_FOUND",
    "message": "User with email \"user@example.com\" was not found."
}
```

Possible errors are: `USER_NOT_FOUND`, `PASSWORD_MISMATCH`.

If internal server error then same as above, but with status code 500 and error `INTERNAL_SERVER_ERROR`.

## Refresh Token Flow

Request is made to auth by client when `accessToken` has expired.

Request:
```
POST: https://auth.musicstream.app/v1/refresh
Cookie: refreshToken=...

Body:
{}
```

Auth should check that session with this id exists and is still valid. If so, new access token is issued.

Response:
```
Status code: 200

Body:
{
    "accessToken": "..."
}
```

If session expired/does not exist:
```
Status code: 400

Body:
{
    "error": "SESSION_EXPIRED"
}
```

Possible errors are: `SESSION_EXPIRED`, `SESSION_NOT_FOUND`. In case of such errors, frontend should redirect user to auth page.

## Set Telegram ID flow.

Request:
```
POST: https://auth.musicstream.app/v1/telegramID
Authorization: Bearer <accessToken>

Body:
{
    "telegramID": 42
}
```

Auth should update `telegram_id` field in firestore with this value.

Response if everything is ok:
```
Status code: 200

Body:
{}
```

If access token is invalid:
```
Status code: 400

Body:
{
    "error": "TOKEN_INVALID"
}
```

If access token has expired, then `error` is `TOKEN_EXPIRED`.

If internal server error, then `error` is `INTERNAL_SERVER_ERROR`.