# How to sign commits

[Fun post on Habr related to this topic](https://habr.com/ru/post/515550/).

GPG - [Wikipedia](https://en.wikipedia.org/wiki/GNU_Privacy_Guard)

Signing commits is needed to proove that it was you who committed certain piece of code.

GPG is based on public-key cryphtograhpy. That means that you have two keys:
- private (treat it as a password) - stored on your computer and is used to sign commits.
- public (you place it in your Github/Gitlab profile, give to other people etc.) - used by other people to verify that it is your signature, not someone's else.

Actually, you can sign emails, files and basically whatever you like. You can also upload your PGP public key to [Keybase](https://keybase.io), which is another way to share your public key and prove that you own Github, Reddit profiles or a website.

## Step 1: Generate GPG key

Follow [this guide by Github](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/generating-a-new-gpg-key).

## Step 2: Import to Gitlab

Go to [profile > GPG Keys](https://gitlab.com/profile/gpg_keys) and paste your public key to the `Key` area.

## Step 3: Tell git to sign commits

```
git config commit.gpgsign true
```

## Step 4: Check that your newly pushed commits are signed

You should see something like this:

![verified commit example](resources/1.png)