# Development Flow

See the bottom of the document for FAQs.

## Step 1: Create a new branch from master

We have **master** branch in each repository. Standard flow is when you start working on a new feature, create a new branch:

```
# verify that you are on master
git checkout master

# pull updates
git pull origin master

# create a new branch
git checkout -b branch-name
```

Branch name should describe feature you are implementing or the bug you are fixing.

Examples of **bad** branch names:
```
some-fixes
new-feature
wip
asdf
```

Examples of **good** branch names:
```
add-playlists
sync-playback
fix-password-validation
```

## Step 2: Hack...

While hacking around you may want to update your branch with latest changes from master. You can do it like this:

```
git fetch
git rebase master
```

If there are any conflicts, git will prompt you with a message.

**Note**: do not forget to increment version in `package.json` when working on backend services. It is required to roll update to Kubernetes cluster after MR is merged.

## Step 3: Create MR (merge request)

Quite simple:

```
git push origin branch-name 
```

After that follow the link you will be given and create a merge request.

After making sure all CI checks pass, please send a link to this MR to our group or to `@nikitavbv` private message. It is strongly encouraged to send MRs to group though. It shouldn't be the one person who reviews all the MRs :) But it is up to you when to send to group or to private message. When sending MR to the group, please provide short description of changes you are making.

## Step 4: Wait for approvals, review other MRs

Wait until someone approves your MR. Meanwhile you can review other MRs! :)

If an approved MR is not merged in a while, please ping `@nikitavbv` (unluckily in our current setup only one person has merge rights).

## Step 5: MR is merged

Congratulations! Don't forget to update your local `master` branch.

# FAQ

## Why don't we have `dev` branch?

You want a `dev` branch when broken code in `master` is totally unacceptable. In our case it is okay to have broken code in `master`. Firstly, we do not deploy on each commit in master (only when image with new tag is built). And we do not build an actual production system, so it is okay even if we deploy broken code :)

In other words, `dev` branch seems not worth it. Let's create a separate branch for each feature/fix and merge it to master when ready.

## Why is it a good idea to review each other MRs?

We build this project for learning. And what is good for learning? Sharing knowledge.

While reviewing merge request you learn more about the system we are building, you learn more about coding in TypeScript in general. While reviewing MR you may get an interesting idea or find a more optimal solution that author of the MR is suggesting.

Most importantly, you develop your skills to read and undestand other people code. Don't underestimate this skill. 

Reviewing merge requests and sending your merge requests for review is not a thing you absolutely should do, but it is strongely encouraged.

## How to review a merge request?

It depends on how much time you have and how deep you want to go.

When first looking at an MR, try to understand general structure. What is the point of this MR? Does it introduce new feature? Does it fix some bugs? Which files are changed? It project structure changed in any way?

Wanna continue looking at this MR? Well, now try to understand how this new feature works. If it is a bug, try to understand what was causing it and what it the fix. Feel free to contact author if you have any questions. If there is a doc for this feature, it is a good idea to check if implementation matches description. Got the undestanding of how things work? Now it is okay to approve an MR :)

Still have some time? Go through the implementation and check it. Can it break under certain conditions? Edge cases? Are all possible scenarious handled? Are exceptions handled? Are there any possible performance issues?

Want to go even deeper? Given a feature description, imagine for a second how you would implement this. Does your solution match the one proposed my MR author?

Don't be too strict though. Please remember that there are multiple solutions available for each problem and there are small things which are subject to a preference.

If you don't understand anything while reading a merge request, then it is certainly a red flag. Kindly ask author to explain their code, refactor it or add some comments.

## When to approve a MR?

If you think that this merge request does not make codebase worse (in almost all cases it doesn't), then approve it.