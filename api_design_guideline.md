# API design guideline

## Protocol

We use HTTP for all the communication. Services which require real time updates (i.e. playback) use websocket.

## Request / Response Format

All requests and responses are in JSON format. This includes client-server communication and communication between microservices.

## API Resources

We use resource-oriented API. That means that there is a resource hierarchy, where each node is either a simple resource or a collection resource.

A collection resource contains a list of resources of the same type. Example: `/songs`

A simple resource has some state and zero os more sub-resource. For examle: `/songs/{id}/lyrics`

TO sum up, we use REST-style api.

## Resource naming

We use plural nouns: `songs`, `playlists`. If resource contains multiple words, we use `-` as a delimiter.

## Request/Response payload

All field names use snake case.

In case resource requires authorization, `x-access-token` header should be set in request.

Response must contain body, which is a json object. Body should always contain `status` field. In case of successful operation it should be set to `ok`.

We use `POST` requests for resource creation. After resource is created succesfully, response payload should contain resource id set in `id` field.

### Other possible statuses:
- `MISSING_FIELD` - in such case should contain `field_name` mentioning the field that is missing.
- `INCORRECT_FIELD_VALUE`. Should contain `field_name` mentioning which field has incorrect value and `error_description` explaining what's wrong with field value.
- `INTERNAL_SERVER_ERROR`.
- `RATE_LIMITED` - in case maximum allowed requests per second is reached.
- `UNAUTHORIZED` - in case `x-access-token` was not set.

### Pagination:
If resource support paginated requests, the following fields must be set in response:

`pagination.offset` - current offset.

`pagination.entries` - total entries in this batch.

`pagination.next_page_available` - if there are more entities for next batch.

As for request, `offset` and `limit` can be set in query string. If those are not set, default values should be used.

### Status Codes
- `200` if request was succesfully proccessed.
- `201` if new resource was created as the result of this request.
- `400` if some field is not set or set to incorrect value.
- `401` if `x-access-token` field is not set.
- `403` if token is set, but user does not have access to the requested resource.
- `404` if requested resource was not found (including being recently deleted).
- `413` if uploaded file is bigger than maximum allowed by the storage service.
- `429` if too many requests were performed by this client in a unit of time. Actual rate limiting depends on the implementation of each service.
- `500` if internal server error.
- `502` if load balancer fails to communicate to backend service.
- `504` if backend service does not reply to load balancer in time.

### Non-standard methods

Non-standard methods should only be used for functionality that cannot be easily expressed via standard methods.

Custom methods should use the following mapping:

`https://service.musicstream.app/v1/resource/name:customVerb`

Please note that custom methods should use HTTP POST, except for methods which are alternative get, where HTTP GET may be used (in this case they should not have any side effects).

Example use case: search songs, force lyrics fetch.

Common custom methods for our use cases are `:search` and `:move` (this moves resource between collections, for example song between playlists).

### WebSocket messages

As for websocket messages, each message (doesn't matter if it was produced by server or client) should contain `id` field (increments by 1 on each message starting from 0) and `action` field (this field defines how message will be processed). After sending websocket message, the other side should acknowledge that this message is proccessed by sending reply message with the same `id` and `action` set to `ack`.

There is also a ping message (`action` set to `ping`). It should be sent by client every 10 seconds. If no ping messages are sent by client in the window of 2 minutes, this connection can be closed from the server side.

## Versioning

Version is an integer preceded with letter `v`, for example: `v1`. It should go as the first part of the url.

Note that api version prefix should not be set in service code. Instead it should be handled by url rewrite at ingress controller level.