# Install gcloud

`gcloud` is a useful tool for all things related to GCP.

Note that it requires Java 8+ insalled.

To install `gcloud` on Debian or Ubuntu:
```
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

sudo apt-get install apt-transport-https ca-certificates gnupg

curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

sudo apt-get update && sudo apt-get install google-cloud-sdk
```

On Windows:

Use [this installer](https://dl.google.com/dl/cloudsdk/channels/rapid/GoogleCloudSDKInstaller.exe).

# Run Firestore emulator

```
gcloud beta emulators firestore start
```

After that start your service with something like this:
```
FIRESTORE_EMULATOR_HOST=localhost:8808 npm start
```

(tested on Linux, if something is different for Windows, please edit this doc)

# How to connect to Postgres locally

When developing services locally, you may want to connect to our postgres server. Of course, you can run a local database inside a docker container, but the easiest way to test your code is to connect to the actual *production* database.

Let's imagine, you need to run some serivce locally which requires connection to DB. For example, `storage`.

First, you need host and password. Please contact `@nikitavbv` and he will send those to you.

After you have those, you need to add some variables to `.env` file:
```
DATABASE_CONNECTION_STRING=postgres://api:password@host/postgres
AUTH_SECRET=somerandomstring
```

After that your service should start normally and connect to actual database.

For debug purposes or to edit a schema, you may use DataGrip with the same credentials.
